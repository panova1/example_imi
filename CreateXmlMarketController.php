<?php
/**
 * CreatexmlmarketController Класс

* Создание xml-файла для ЯндексМаркет.
* Возвращает строку с именем функции обработчика и параметром 'param1' = 1 при успешном формировании файла, иначе - 0
*/
class CreateXmlMarketController extends CustomControllerAction {
  public function indexAction() {
    if ($this->createYMLmarket($this->db)) {
      $mess = 1;
    } 
    else {
      $mess = 0;
    } 
    echo $_GET['callback']."({param1:$mess});";
  }
        
  /**
  * Формирование xml-документа
  *
  * @param object $_db текущая БД
  */   
  public function createYMLmarket($_db) {

    $dom = new DomDocument('1.0','windows-1251');
     
    $yml_catalog = $dom->appendChild($dom->createElement('yml_catalog'));
    $yml_catalog->setAttribute('date', date('Y-m-d H:i'));
     
    $shop = $yml_catalog->appendChild($dom->createElement('shop'));
    
/*-----------------------------------------------------------------------------------------------------------------*/                             
/*Описание магазина*/                                                            
/*-----------------------------------------------------------------------------------------------------------------*/ 
    $name = $shop->appendChild($dom->createElement('name'));
    $name->appendChild($dom->createTextNode(iconv('windows-1251','UTF-8', "Официальный дистрибьютор")));

    $company = $shop->appendChild($dom->createElement('company'));
    $company->appendChild($dom->createTextNode(iconv('windows-1251','UTF-8',$this->company_name_string)));
     
    $url_site = $shop->appendChild($dom->createElement('url'));
    $url_site->appendChild($dom->createTextNode($this->site));

    $email_support = $shop->appendChild($dom->createElement('email'));
    $email_support->appendChild($dom->createTextNode(iconv('windows-1251','UTF-8',$this->site_support)));
     
    $currencies = $shop->appendChild($dom->createElement('currencies'));
    $currency = $currencies->appendChild($dom->createElement('currency'));
    $currency->setAttribute('id', 'RUR');
    $currency->setAttribute('rate', '1');
     
/*-----------------------------------------------------------------------------------------------------------------*/                             
/*Категориии*/                                                            
/*-----------------------------------------------------------------------------------------------------------------*/ 
    $categories = $shop->appendChild($dom->createElement('categories'));
                 
    $yml_counter = 1;
    foreach($this->all_category as $k => $v) {
      $category = $categories->appendChild($dom->createElement('category'));
      $category->setAttribute('id', $yml_counter);
      $category->appendChild($dom->createTextNode(iconv('windows-1251','UTF-8',$v)));
                 
      $category_yml[$v] = $yml_counter;
      $yml_counter++;                       
    }                 
        
/*-----------------------------------------------------------------------------------------------------------------                            
/*Доставка
/*upd: изменены параметры доставки - 1 день и 500 рублей */                                                          
/*-----------------------------------------------------------------------------------------------------------------*/          
    $deliveryOptionsTop = $shop->appendChild($dom->createElement('delivery-options'));
    $deliveryOptionTop = $deliveryOptionsTop->appendChild($dom->createElement('option'));
    $deliveryOptionTop->setAttribute('cost', '500');
    $deliveryOptionTop->setAttribute('days', '1');

/*-----------------------------------------------------------------------------------------------------------------*/                             
/*Товарные предложеия*/                                                            
/*-----------------------------------------------------------------------------------------------------------------*/         
    $offers = $shop->appendChild($dom->createElement('offers'));
                 
    $product = DatabaseObject_Adminka::getProductAllYML($_db);               

    for($i=0;$i<count($product);$i++) {

      $this->getTextXML_YML($product[$i]['id_offer'], $this->setLink($product[$i]), $category_yml, $offers, $dom, $product[$i]);  
    }
     
    try {                          
      $dom->formatOutput = true; // установка атрибута formatOutput
      $test1 = $dom->saveXML(); 
      $dom->save('yml_webmaster.xml'); // сохранение файла 

      return true;                    
    } 
    catch (Exception $e) {
        return false;
    }
  
  }
     
  /**
  * Формирование узла продукта xml-документа
  *
  * @param int $id 
  * @param string $url 
  * @param array $category_yml 
  * @param array $offers 
  * @param obj $temp_dom 
  * @param array $product_all_data 
  */          
  public function getTextXML_YML($id, $url, $category_yml, $offers, $temp_dom, $product_all_data) {

    $price = $product_all_data['price'];
    $oldprice = $product_all_data['price_old'];
    $category = $product_all_data['category_2']; 
    $subcategory = $product_all_data['subcategory'];
    $brend = $product_all_data['brend'];
    $articul = $product_all_data['articul'];
    $picture = $this->_helper->all->translitRusToEng($brend).'/'.$articul.'.jpg'; 
    $model = $product_all_data['name'].' '.str_replace('прочее','',$product_all_data['subname']); 
    $guarantee = str_replace('прочее','',$product_all_data['guarantee']);
    $in_supplier = $product_all_data['articul'];
    $in_sklad = $product_all_data['in_sklad']; 
    $in_sklad_do_not_change = $product_all_data['in_sklad_do_not_change'];
    $country = $product_all_data['country']; 
    $weight = $product_all_data['weight']; 

/*-----------------------------------------------------------------------------------------------------------------*/                             
/*Расчет доставки*/                                                            
/*-----------------------------------------------------------------------------------------------------------------*/ 
    $raschet_count_days_delivery = '';
    $raschet_count_days_delivery = $this->deliveryInterval($product_all_data);
     
/*-----------------------------------------------------------------------------------------------------------------*/                             
/*Вывод товара, доступного для заказа*/                                                            
/*-----------------------------------------------------------------------------------------------------------------*/                 
    if ($raschet_count_days_delivery) {
                
      $offer = $offers->appendChild($temp_dom->createElement('offer'));                   
      $offer->setAttribute('id',$id);
      $offer->setAttribute('type','vendor.model');
    
      if ($this->all_ostatki_supplier[strtoupper($brend)][strtoupper($articul)] || $in_sklad || $in_shop) {
        $offer->setAttribute('available','true');
      }
      else {
        $offer->setAttribute('available','false');
      }
      
      //ссылка
      $url_ind = $offer->appendChild($temp_dom->createElement('url'));
      $url_ind->appendChild($temp_dom->createTextNode($url));
      
      //цены
      $price_ind = $offer->appendChild($temp_dom->createElement('price'));
      $price_ind->appendChild($temp_dom->createTextNode($price));   
      
      if (trim($oldprice) && trim($oldprice)>$price) {
        $oldprice_ind = $offer->appendChild($temp_dom->createElement('oldprice'));
        $oldprice_ind->appendChild($temp_dom->createTextNode($oldprice));                    
      }
      
      //валюта
      $currencyId_ind = $offer->appendChild($temp_dom->createElement('currencyId'));
      $currencyId_ind->appendChild($temp_dom->createTextNode('RUR')); 
      
      $categoryId_ind = $offer->appendChild($temp_dom->createElement('categoryId'));
      $categoryId_ind->appendChild($temp_dom->createTextNode($category_yml[$category])); 
      
      //изображение
      $picture_ind = $offer->appendChild($temp_dom->createElement('picture'));
      $picture_ind->appendChild($temp_dom->createTextNode($picture)); 
      
      //доставка                    
      $delivery_ind = $offer->appendChild($temp_dom->createElement('delivery'));
      $delivery_ind->appendChild($temp_dom->createTextNode('true'));  
      
      $delivery_ind_options = $offer->appendChild($temp_dom->createElement('delivery-options'));
      $delivery_ind_option_before = $delivery_ind_options->appendChild($temp_dom->createElement('option'));
      $delivery_ind_option_after = $delivery_ind_options->appendChild($temp_dom->createElement('option'));
                        
      $delivery_ind_option_before->setAttribute('days',$raschet_count_days_delivery['before'] ? $raschet_count_days_delivery['before'] : date("d-m-Y" ,strtotime("now +30 days")));
      $delivery_ind_option_before->setAttribute('order-before','14');
      $delivery_ind_option_before->setAttribute('cost', $this->raschetDelivery($price, $brend, $category));
      
      $delivery_ind_option_after->setAttribute('days',$raschet_count_days_delivery['after'] ? $raschet_count_days_delivery['after'] : date("d-m-Y" ,strtotime("now +30 days")));
      $delivery_ind_option_after->setAttribute('order-before','24');
      $delivery_ind_option_after->setAttribute('cost', $this->raschetDelivery($price, $brend, $category));
      
      //категория                                                                                                             
      $typePrefix_ind = $offer->appendChild($temp_dom->createElement('typePrefix'));
      $typePrefix_ind->appendChild($temp_dom->createTextNode(iconv('windows-1251','UTF-8',$category.' '.$subcategory))); 
      
      //вендор                                        
      $vendor_ind = $offer->appendChild($temp_dom->createElement('vendor'));
      $vendor_ind->appendChild($temp_dom->createTextNode(iconv('windows-1251','UTF-8',$brend)));                                                        
      $vendorCode_ind = $offer->appendChild($temp_dom->createElement('vendorCode'));
      $vendorCode_ind->appendChild($temp_dom->createTextNode(iconv('windows-1251','UTF-8',$articul))); 
      
      //модель                                     
      $model_ind = $offer->appendChild($temp_dom->createElement('model'));                   
      $model_ind->appendChild($temp_dom->createTextNode(iconv('windows-1251','UTF-8', $model_spec_name.' ('.$articul.')')));                                                                    
      //описание оплаты в зависимости от наличия    
      if($in_supplier || $in_sklad || $in_sklad_do_not_change) {
          $sales_notes_ind =  $offer->appendChild($temp_dom->createElement('sales_notes'));
          $sales_notes_ind->appendChild($temp_dom->createTextNode(iconv('windows-1251','UTF-8','Оплата: VISA / MASTERCARD / Безнал / Наличные'))); 
      }
      else {
          $sales_notes_ind =  $offer->appendChild($temp_dom->createElement('sales_notes'));
          $sales_notes_ind->appendChild($temp_dom->createTextNode(iconv('windows-1251','UTF-8','Предоплата: VISA / MASTERCARD / Безнал / Наличные'))); 
      }
      
      $manufacturer_warranty_ind = $offer->appendChild($temp_dom->createElement('manufacturer_warranty'));
      $manufacturer_warranty_ind->appendChild($temp_dom->createTextNode('true')); 
      
      //страна-производитель
      $country_of_origin_ind = $offer->appendChild($temp_dom->createElement('country_of_origin'));
      
      if (!trim($country)) {
        if ($guarantee=='прочее' || !$guarantee) {
          $country = 'Германия';
        }
        else {
          if (trim(substr($guarantee, 28))) {
            //для фразы "5 лет, страна производитель Германия"
            $country = trim(substr($guarantee, 28));
          }
          else {
            //для фразы "5 лет, сделано в Германии"
            $country = substr(trim(substr($guarantee, 17)), 0, strlen(trim(substr($guarantee, 17))) - 1) . 'я';
          }
        }
      }
      else {
        if (strpos($country,'-')){
          $c_temp = explode('-' , $country);
          $country = $c_temp[0];
        }
      }
                                         
      if (strtolower($country)!='франция') {
        $country_of_origin_ind->appendChild($temp_dom->createTextNode(iconv('windows-1251','UTF-8', $country) )); 
      }
                      
      //вес                          
      $weight_ind_text = trim(str_replace(',', '.', str_replace('кг', '', str_replace('вес:', '', $weight))));

      if ($weight_ind_text && $weight_ind_text!='прочее') {
       $weight_ind = $offer->appendChild($temp_dom->createElement('weight'));
       $weight_ind->appendChild($temp_dom->createTextNode(iconv('windows-1251','UTF-8', $weight_ind_text))); 
      }          

    }
  } 
                             
  /**
  * Расчет времени доставки товара
  *
  * @param array $product 
  */               
  public function deliveryInterval($product) { 

    $this->all_ostatki_supplier[$product['brend']][$product['articul']] ? $product['term'] = $this->all_ostatki_supplier[$product['brend']][$product['articul']]['term'] : '' ;
    // если есть у поставщика и нет на складе
    if ((!$product['in_sklad'] && !$product['in_sklad_do_not_change'] && !$product['in_shop']) && $product['term']) {
      $res['before'] = $this->getDaysOffForMarketOnlyCount($product['term'], '','',1,'ymd');
      $res['after'] = $this->getDaysOffForMarketOnlyCount($product['term'], '','',2,'micro');
    }
    // если есть на складе
    elseif ($product['in_sklad'] || $product['in_sklad_do_not_change'] || $product['in_shop']) {
      $res['before'] = $this->getDaysOffForMarketOnlyCount(0, '','',1,'micro');
      $res['after'] = $this->getDaysOffForMarketOnlyCount(0, '','',2,'micro');
    }

    return $res;
  } 
    
  /**
  * Расчет стоимости доставки товара в зависимости от цены, бренда и категории
  *
  * @param int $price 
  * @param string $brend //нет в условиях
  * @param string $category //нет в условиях
  */    
  public function raschetDelivery($price,$brend,$category) {
             
    if ($price<2000) {
      $local_delivery_cost = 1000;
    }
    elseif ($price<20000) {
      $local_delivery_cost = 500;
    }
    else {
      $local_delivery_cost = 0;
    }
                      
    return $local_delivery_cost;
  }
}
